<?php
	require_once('conexion.php'); 

	#ltrayendo los datos de la tabla a los inputs
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		
		$query = "Select * from task where id = $id";
		
		$result = mysqli_query($con,$query);
		
		if(mysqli_num_rows($result)==1){
			
			$row = mysqli_fetch_array($result);
			$title = $row['title'];
			$description = $row['description'];
		}
	}

	#Obteniendo los nuevos datos de los inputs y actualizando
	if(isset($_POST['btnupdate'])){
		
		$id = $_GET['id'];
		$title = $_POST['txttitle'];
		$description = $_POST['tadescription'];
		
		$query = "update task set title = '$title', description='$description' where id = $id";
		mysqli_query($con,$query);
		
		$_SESSION['message'] = 'Task Updated succesfully';
		$_SESSION['message-type'] = 'warning';
		header('Location: index.php');
	}

?>

<?php require_once('includes/header.php'); ?>

<div class="container p-4">
	<div class="row">
		<div class="col-md-4 mx-auto">
			<div class="card card-body">
				<form action="edit-task.php?id=<?php echo $_GET['id']; ?>" method="post">
					<div class="form-group">
						<input type="text" name="txttitle" value="<?php echo $title; ?>" class="form-control" placeholder="Update Title">
					</div>
					<div class="form-group">
						<textarea name="tadescription" rows="2" class="form-control" placeholder="Update Description"><?php echo $description; ?></textarea>
					</div>
					<button class="btn btn-success btn-block" name="btnupdate">Update Task</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>	
