<?php require_once('conexion.php'); ?>

<?php require_once('includes/header.php'); ?>


	<div class="container p-4">
		<div class="row">
			<!-- card para registrar las tareas-->
			<div class="col-md-4">
			
			<?php if(isset($_SESSION['message'])){ ?>
			
			<!-- alerta -->
			<div class="alert alert-<?= $_SESSION['message-type'];?> alert-dismissible fade show" role="alert">
			  <?= $_SESSION['message']; ?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>

			<?php session_unset(); #limpiando los datos que hay en la sesion 
			} ?>
				<div class="card card-body">
					<form action="save-task.php" method="post">
						<div class="form-group">
							<input type="text" name="txttitle" class="form-control" placeholder="Task Title" autofocus>
						</div>
						<div class="form-group">
							<textarea name="tadescription" rows="2" class="form-control" placeholder="Task Description"></textarea>
						</div>
						<input type="submit" class="btn btn-success btn-block" name="btnsavetask" value="Save Task">
					</form>
				</div>
			</div>
			
			<!-- tabla para listar registros -->
			<div class="col-md-8">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Title</th>
							<th>Description</th>
							<th>Created At</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php 						
							$query = "Select * from task";
							
							$resultTask = mysqli_query($con,$query);
							
							while($row = mysqli_fetch_array($resultTask)){
						?>
						
						<tr>
							<td><?php echo $row['title']; ?></td>
							<td><?php echo $row['description']; ?></td>
							<td><?php echo $row['created_at']; ?></td>
							<td>
								<a href="edit-task.php?id=<?php echo $row['id']?>" class="btn btn-secondary"><i class="fas fa-marker"></i></a>
								<a href="delete-task.php?id=<?php echo $row['id']?>" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
							</td>
						</tr>
						
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?php require_once('includes/footer.php'); ?>	
